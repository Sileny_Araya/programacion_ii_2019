/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestion.de.productos;

import java.util.Scanner;

/**
 *
 * @author Sileny Araya
 */
public class ProductosRefrigerados extends Productos{

    int codigoOrganismoSupervision, temperaturaMantenimiento;
    String fechaEnvasado,paisOrigen;

    public ProductosRefrigerados( String fechaCaducidad, String numeroLote,int codigoOrganismoSupervision, int temperaturaMantenimiento, String fechaEnvasado, String paisOrigen) {
        super(fechaCaducidad, numeroLote);
        this.codigoOrganismoSupervision = codigoOrganismoSupervision;
        this.temperaturaMantenimiento = temperaturaMantenimiento;
        this.fechaEnvasado = fechaEnvasado;
        this.paisOrigen = paisOrigen;
    }
    
    public String getAtributos() {
        return "Fecha de Caducidad: " + fechaCaducidad
                + "\nNumero de Loto: " + numeroLote
                + "\nCodigo del organismo de supervisión alimentaria :" + codigoOrganismoSupervision
                + "\nTemperatura de mantenimiento recomendada :" + temperaturaMantenimiento
                + "\nFecha de envasado, :" + fechaEnvasado
                + "\nPais de Origen: " + paisOrigen;
    }
    
    public void datosProdRefri (){
        Scanner sc = new Scanner(System.in);
            System.out.println("\nIntroduzca la fecha de caducidad del producto: ");
            fechaCaducidad = sc.next();
            System.out.println("Introduzca el numero de lote del producto: ");
            numeroLote = sc.next();
            System.out.println("Introduzca el codigo del organismo de supervisión alimentaria del producto: ");
            codigoOrganismoSupervision = sc.nextInt();
            System.out.println("Introduzca el codigo la temperatura de mantenimiento recomendada del producto: ");
            temperaturaMantenimiento = sc.nextInt();
            System.out.println("Introduzca la fecha de envasado del producto: ");
            fechaEnvasado = sc.next();
            System.out.println("Introduzca el pais de origen del producto: ");
            paisOrigen = sc.next();
            
            System.out.println("\nProducto ingresado correctamente ");
            
    }
    
     //polimorfismo
    public void tipoProducto() {
        System.out.println("El Producto es refrigerado");
    }
    
}
