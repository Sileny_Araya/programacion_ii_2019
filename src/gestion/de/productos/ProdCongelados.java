/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestion.de.productos;

import java.util.Scanner;

/**
 *
 * @author Sileny Araya
 */
public class ProdCongelados extends Productos{
    
    String fechaEnvasado,paisOrigen;
    int temperaturaMantenimiento;

    public ProdCongelados(String fechaCaducidad, String numeroLote, String fechaEnvasado, String paisOrigen, int temperaturaMantenimiento) {
        super(fechaCaducidad, numeroLote);
        this.fechaEnvasado = fechaEnvasado;
        this.paisOrigen = paisOrigen;
        this.temperaturaMantenimiento = temperaturaMantenimiento;
    }

 public String getAtributos() {
        return "Fecha de Caducidad: " + fechaCaducidad
                + "\nNumero de Lote: " + numeroLote
                + "\nFecha de Envasado:" + fechaEnvasado
                + "\nPais de Origen: " + paisOrigen
                + "\nTemperatura de Mantenimiento: " + temperaturaMantenimiento;
    }
         
    private void ProdCongeladoAire(){
        int porceNitro, porceDioxi, porceVaporAgu;

        System.out.println("*-*-*-*-*Informacion para productos congelados por aire-*-*-*-*");
        Scanner sc = new Scanner(System.in);
        System.out.println("\nIntroduzca el pocentaje de nitrogeno del producto: ");
        porceNitro = sc.nextInt();
        System.out.println("Introduzca el porcentaje de dioxido de carbono del producto: ");
        porceDioxi = sc.nextInt();
        System.out.println("Introduzca el porcentaje de vapor de agua del producto: ");
        porceVaporAgu = sc.nextInt();
        System.out.println("Producto agregado correctamente");
        tipoProducto();
    }

    private void ProdCongeladoAgua() {
        int gramosSal, litroAgua;
        System.out.println("*-*-*-*-*Informacion para productos congelados por agua-*-*-*-*");
        Scanner sc = new Scanner(System.in);
        System.out.println("\nIntroduzca los gramos de sal que lleva el producto: ");
        gramosSal = sc.nextInt();
        System.out.println("Introduzca los litros de agua que lleva el producto: ");
        litroAgua = sc.nextInt();
        System.out.println("Producto agregado correctamente");
        tipoProducto();
    }
     private void ProdCongeladoNitrogeno (){
         String metodoCongelado;
         int tiempoExposicion;
        System.out.println("*-*-*-*-*Informacion para productos congelados por nitrogeno-*-*-*-*");
        Scanner sc = new Scanner(System.in);
        System.out.println("\nIntroduzca el tipo de metodo de cogelamiento del producto: ");
        metodoCongelado = sc.next();
        System.out.println("Introduzca el tiempo de exposición al nitrógeno expresada en segundos : ");
        tiempoExposicion = sc.nextInt();
        System.out.println("Producto agregado correctamente");
        tipoProducto();
     }

    //polimorfismo
    public void tipoProducto() {
        System.out.println("El producto es congelado");
    }
    
    public void datosProdCongelados (){
        Scanner sc = new Scanner(System.in);
            System.out.println("\nIntroduzca la fecha de caducidad del producto: ");
            fechaCaducidad = sc.next();
            System.out.println("Introduzca el numero lote del producto: ");
            numeroLote = sc.next();
            System.out.println("Introduzca la fecha de envasado del producto: ");
            fechaEnvasado = sc.next();
            System.out.println("Introduzca el pais de origen del producto: ");
            paisOrigen = sc.next();
            System.out.println("Introduzca la de temperatura de mantenimiento del producto: ");
            temperaturaMantenimiento = sc.nextInt();
            Scanner ingresar = new Scanner(System.in);
        while (true) {
            System.out.println("1.Productos congelados por aire,\n2.Productos congelados por agua"
                    + "\n3.Productos congelados por nitrogeno\n4.Salir\n"
                    + "\nSeleccione una opción: ");
            int opcion = ingresar.nextInt();
            switch (opcion) {
                case 1:
                    ProdCongeladoAire();
                case 2:
                    ProdCongeladoAgua();
                case 3:
                    ProdCongeladoNitrogeno();
                default:
                    System.out.println("Error! Digite opción válida!!");
                    break;
            }

            System.out.println("\nProducto ingresado correctamente ");
        }

}
    }
