/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

import java.util.Scanner;

/**
 *
 * @author Sileny Araya
 */
public class Masajista extends SeleccionFutbol{
    
    public String titulacion;
    public int añosExperiencia;

    public Masajista(int id, String nombre, String apellidos, int edad,String titulacion, int añosExperiencia) {
        super(id, nombre, apellidos, edad);
        this.titulacion = titulacion;
        this.añosExperiencia = añosExperiencia;
    }
    
    //polimorfismo
    public void tipoPersona() {
        System.out.println("la persona es masajista");
    }
    
     
     public void darMasaje (){
         System.out.println("Al masajista "+ nombre+ " le toca dar un masaje despues del entrenamiento de mañana");
         SeleccionFutbol s =new SeleccionFutbol(0, " ", " ", 0);
         s.tipoPersona();
     }

    
    public void datosMasi (){
        Scanner sc = new Scanner(System.in);
            System.out.println("\nIntroduzca el id del masajista: ");
            id = sc.nextInt();
            System.out.println("Introduzca el nombre del masajista: ");
            nombre = sc.next();
            System.out.println("Introduzca el apellido del masajista: ");
            apellidos = sc.next();
            System.out.println("Introduzca la edad del masajista: ");
            edad = sc.nextInt();
            System.out.println("Introduzca la titulacion del masajista: ");
            titulacion = sc.next();
            System.out.println("Introduzca los años de experiencia del masajista: ");
            añosExperiencia = sc.nextInt();
            
            System.out.println("\nMasajista ingresado correctamente ");
            
    }
}
